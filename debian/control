Source: elph
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               help2man
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/elph
Vcs-Git: https://salsa.debian.org/med-team/elph.git
Homepage: https://www.cbcb.umd.edu/software/ELPH/
Rules-Requires-Root: no

Package: elph
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: DNA/protein sequence motif finder
 ELPH (Estimated Locations of Pattern Hits) is a general-purpose
 Gibbs sampler for finding motifs in a set of DNA or protein sequences.
 The program takes as input a set containing anywhere from a few dozen to
 thousands of sequences, and searches through them for the most common
 motif, assuming that each sequence contains one copy of the motif. ELPH
 was used to find patterns such as ribosome binding sites (RBSs) and exon
 splicing enhancers (ESEs).
